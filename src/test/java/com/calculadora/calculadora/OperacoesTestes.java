package com.calculadora.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OperacoesTestes {

    @Test
    public void testarOperacaoDeSoma(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(Operacao.soma(numeros),6);
    }

    @Test
    public void testarSomaDoisNumeros(){
        Assertions.assertEquals(Operacao.soma(1,1),2);
    }

    @Test
    public void testarSubtracaoDoisNumeros(){
        Assertions.assertEquals(Operacao.subtracao(2,1),1);
    }

    @Test
    public void testarSubtracaoLista(){
        List<Integer> numeros = new ArrayList<>();

        numeros.add(5);
        numeros.add(10);
        numeros.add(2);

        Assertions.assertEquals(Operacao.subtracao(numeros),-17);
    }

    @Test
    public void testarMultiplicarDoisNumeros(){
        Assertions.assertEquals(Operacao.multiplicacao(3,2),6);
    }

    @Test
    public void testarMultiplicarLista(){
        List<Integer> numeros = new ArrayList<>();

        numeros.add(3);
        numeros.add(2);
        numeros.add(6);

        Assertions.assertEquals(Operacao.multiplicacao(numeros),36);
    }

    @Test
    public void testarDivisaoDoisNumeros(){
        Assertions.assertEquals(Operacao.divisao(100,10),10);
    }
}