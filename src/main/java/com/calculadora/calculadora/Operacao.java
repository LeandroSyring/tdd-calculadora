package com.calculadora.calculadora;

import java.util.Collections;
import java.util.List;

public class Operacao {

    public static int soma(int numUm, int numDois){
        int resultado = numUm + numDois;
        return resultado;
    }

    public static int soma(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero:numeros) {
            resultado += numero;
        }
        return resultado;
    }

    public static int subtracao(int numUm, int numDois){
        int resultado = numUm - numDois;
        return resultado;
    }

    public static int subtracao(List<Integer> numeros){
        Integer resultado = 0;
        for (Integer numero:numeros) {
            resultado -= numero;
        }
        return resultado;
    }

    public static int multiplicacao(int numUm, int numDois){
        return numUm * numDois;
    }

    public static int multiplicacao(List<Integer> numeros){
        Integer resultado = 1;
        for (Integer numero:numeros) {
            resultado *= numero;
        }
        return resultado;
    }

    public static double divisao(int divisor, int dividendo){

        return divisor / dividendo;
    }
}
